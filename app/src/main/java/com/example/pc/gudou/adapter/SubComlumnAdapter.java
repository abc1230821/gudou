package com.example.pc.gudou.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pc.gudou.R;
import com.example.pc.gudou.entity.SubColumnInfo;
import com.example.pc.gudou.util.GlideImgManager;

import java.util.List;

/**
 * Created by Administrator on 2017/6/1.
 */

public class SubComlumnAdapter extends CommonAdapter<SubColumnInfo> {
    Context context;
    public SubComlumnAdapter(Context context, List<SubColumnInfo> list, int layoutId) {
        super(context, list, layoutId);
        this.context = context;
    }

    @Override
    public void convert(ViewHolder holder, SubColumnInfo item, int position) {
        ImageView iv_subcomlumn = holder.getView(R.id.iv_subcomlumn);
        TextView tv_subcomlumn_name  = holder.getView(R.id.tv_subcomlumn_name);
        GlideImgManager.loadImage(context,item.getImageUrl(),iv_subcomlumn);
       // iv_subcomlumn.setImageResource(R.drawable.tab_relation_normal);
        tv_subcomlumn_name.setText(item.getSubColumnName());
    }
}
