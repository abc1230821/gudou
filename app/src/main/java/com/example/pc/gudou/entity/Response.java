package com.example.pc.gudou.entity;

/**
 * Created by Administrator on 2017/5/31.
 */

public class Response extends Entity {
    private String status;
    private String errorMessage;
    private JSonData data;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public JSonData getData() {
        return data;
    }

    public void setData(JSonData data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
