package com.example.pc.gudou.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pc.gudou.R;

/**
 * Created by pc on 2017/5/25.
 */

public abstract class BaseFragment extends Fragment implements View.OnClickListener{
    protected View mView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(mView==null)
        {
            mView=LayoutInflater.from(getContext()).inflate(getLayout(),container,false);
            initViews(mView);
        }

        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    /**
     * @return 需要的显示的布局id
     */
    public abstract int getLayout();

    public abstract void initViews(View view);

    @Override
    public void onClick(View v) {

    }

    public void startActivity(Class clazz) {
        startActivity(new Intent(getContext(),clazz));
        getActivity().overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    protected <T extends View> T findViewById(int resId)
    {
        return (T) mView.findViewById(resId);
    }



    public void onResume() {
        super.onResume();

    }
    public void onPause() {
        super.onPause();

    }

}

