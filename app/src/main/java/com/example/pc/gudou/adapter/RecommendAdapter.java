package com.example.pc.gudou.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pc.gudou.R;
import com.example.pc.gudou.entity.ContentInfo;
import com.example.pc.gudou.entity.ImageUrl;
import com.example.pc.gudou.util.GlideImgManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/6/1.
 */

public class RecommendAdapter extends CommonRecyclerViewAdapter<ContentInfo> {


    public RecommendAdapter(Context context, List<ContentInfo> data) {
        super(context, data);
    }

    public RecommendAdapter(Context context) {
        super(context);
    }

    @Override
    public void convert(CommonRecyclerViewHolder h, ContentInfo entity, int position) {
        TextView tv_content_type = h.getView(R.id.tv_content_type);
        TextView tv_update_time = h.getView(R.id.tv_update_time);
        TextView tv_people_count = h.getView(R.id.tv_people_count);
        TextView tv_content = h.getView(R.id.tv_content);
        ImageView iv_cover = h.getView(R.id.iv_cover);
        setConentTeype(tv_content_type,entity);
        tv_update_time.setText(entity.getUpdateTime());
        tv_people_count.setText(entity.getPeopleCount()+"人看过");
        tv_content.setText(entity.getContentName());
        List<ImageUrl> imageUrls = new ArrayList<>();
        imageUrls = entity.getImageUrlList();

        if(imageUrls.size()>0) {
            //头像
            if(position==0) {
                iv_cover.setVisibility(View.GONE);
            } else {
                GlideImgManager.loadImage(context,imageUrls.get(0).getImageUrl(),iv_cover);
                iv_cover.setVisibility(View.VISIBLE);
            }
        }

    }

    private void setConentTeype(TextView tv_content_type,ContentInfo entity) {
        switch (entity.getContentType()) {
            case 1:
                tv_content_type.setText("图文");
                break;
            case 2:
                tv_content_type.setText("点播");
                break;
            case 3:
                tv_content_type.setText("直播");
                break;
            case 4:
                tv_content_type.setText("纯图");
                break;
            case 5:
                tv_content_type.setText("广告");
                break;
        }
    }

    @Override
    public int getLayoutViewId(int viewType) {
        return R.layout.item_recommend;
    }


}
