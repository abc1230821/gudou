package com.example.pc.gudou.entity;

import java.util.List;

/**
 * Created by Administrator on 2017/5/31.
 */

public class JSonData extends Entity {
        private String columnSize;
        private List<ColumnInfo> columnInfos;
        private List<ContentInfo> contentInfos;
        private List<SubColumnInfo> subColumnInfos;
    public List<ColumnInfo> getColumnInfos() {
        return columnInfos;
    }

    public void setColumnInfos(List<ColumnInfo> columnInfos) {
        this.columnInfos = columnInfos;
    }

    public List<ContentInfo> getContentInfos() {
        return contentInfos;
    }

    public void setContentInfos(List<ContentInfo> contentInfos) {
        this.contentInfos = contentInfos;
    }

    public String getColumnSize() {
        return columnSize;
    }

    public void setColumnSize(String columnSize) {
        this.columnSize = columnSize;
    }

    public List<SubColumnInfo> getSubColumnInfos() {
        return subColumnInfos;
    }

    public void setSubColumnInfos(List<SubColumnInfo> subColumnInfos) {
        this.subColumnInfos = subColumnInfos;
    }
}
