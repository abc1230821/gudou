package com.example.pc.gudou.widgets;
import android.content.Context;

import android.util.AttributeSet;
import android.widget.GridView;

/**
 * 不会滚动的gridview，可嵌入scrollview
 * @author shc
 * @since 2015-01-26
 */

public class NoScrollGridView extends GridView {
public NoScrollGridView(Context context) {
super(context);

}
public NoScrollGridView(Context context, AttributeSet attrs) {
super(context, attrs);
}

@Override
protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,MeasureSpec.AT_MOST);
super.onMeasure(widthMeasureSpec, expandSpec);
}
}