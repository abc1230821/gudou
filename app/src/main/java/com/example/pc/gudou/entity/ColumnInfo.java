package com.example.pc.gudou.entity;

/**
 * Created by Administrator on 2017/5/31.
 */

public class ColumnInfo extends Entity {
    private String columnID;
    private String columnName;
    private int showFlg;
    private int columnType;
    private int sortInx;

    public String getColumnID() {
        return columnID;
    }

    public void setColumnID(String columnID) {
        this.columnID = columnID;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getShowFlg() {
        return showFlg;
    }

    public void setShowFlg(int showFlg) {
        this.showFlg = showFlg;
    }

    public int getColumnType() {
        return columnType;
    }

    public void setColumnType(int columnType) {
        this.columnType = columnType;
    }

    public int getSortInx() {
        return sortInx;
    }

    public void setSortInx(int sortInx) {
        this.sortInx = sortInx;
    }
}
