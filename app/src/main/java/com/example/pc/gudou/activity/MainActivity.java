package com.example.pc.gudou.activity;


import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentTabHost;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.example.pc.gudou.R;
import com.example.pc.gudou.common.BaseActivity;
import com.example.pc.gudou.fragment.DiscoveryFragment;
import com.example.pc.gudou.fragment.HomeFragment;
import com.example.pc.gudou.fragment.MovieFragment;
import com.example.pc.gudou.fragment.MyFragment;
import com.example.pc.gudou.fragment.TVFragment;

/**
 * Created by pc on 2017/5/25.
 */

public class MainActivity extends BaseActivity {
    private FragmentTabHost fragmentTabHost;
    private String texts[] = { "首页", "电视", "片库", "发现", "我的" };
    private int imageButton[] = { R.drawable.tab_relation_selector,
            R.drawable.tab_relation_selector, R.drawable.tab_relation_selector,
            R.drawable.tab_relation_selector ,R.drawable.tab_relation_selector};
    @SuppressWarnings("rawtypes")
    private Class fragmentArray[] = {HomeFragment.class,TVFragment.class,MovieFragment.class,DiscoveryFragment.class,MyFragment.class};




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initTab(savedInstanceState);

    }

    private void initView() {
        System.out.print("fdsf");
    }


    private void initTab(Bundle savedInstanceState) {

        // 实例化tabhost
        fragmentTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        fragmentTabHost.setup(this, getSupportFragmentManager(),
                R.id.maincontent);

        for (int i = 0; i < texts.length; i++) {
            TabHost.TabSpec spec=fragmentTabHost.newTabSpec(texts[i]).setIndicator(getView(i));
            fragmentTabHost.addTab(spec, fragmentArray[i], null);
            //设置背景(必须在addTab之后，由于需要子节点（底部菜单按钮）否则会出现空指针异常)
            // fragmentTabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.bt_selector);
        }
    }


    private View getView(int i) {
        //取得布局实例
        View view=View.inflate(this, R.layout.tabcontent, null);
        //取得布局对象
        ImageView imageView=(ImageView) view.findViewById(R.id.image);
        TextView textView=(TextView) view.findViewById(R.id.text);

        //设置图标
        imageView.setImageResource(imageButton[i]);
        //设置标题
        textView.setText(texts[i]);
        return view;
    }



}

