package com.example.pc.gudou.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pc.gudou.R;
import com.example.pc.gudou.entity.ContentInfo;
import com.example.pc.gudou.util.GlideImgManager;

import java.util.List;

/**
 * Created by Administrator on 2017/6/1.
 */

public class LiveAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final int TYPE_TITLE = 0xff01;
    private static final int TYPE_CONTENT = 0xff02;
    private Context mContext;
    private List<ContentInfo> mData;

    public  LiveAdapter(Context mContext, List<ContentInfo> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from (mContext);
        switch (viewType) {
            case TYPE_TITLE:
                ViewGroup  view_title = ( ViewGroup ) mInflater.inflate ( R.layout.header_title, parent, false );
                return new TitleViewHolder(view_title);
            case TYPE_CONTENT:
                ViewGroup  view_content = ( ViewGroup ) mInflater.inflate ( R.layout.item_live, parent, false );
                return new ContentViewHolder(view_content);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch ( holder.getItemViewType () ) {
            case TYPE_TITLE:
                TitleViewHolder titleViewHolder = ( TitleViewHolder ) holder;
                titleViewHolder.mTitle.setText(mData.get(position).getTitle());
                break;
            case TYPE_CONTENT:
                //GroupViewHolder groupViewHolder = ( GroupViewHolder ) holder;
                //groupViewHolder.mTitle.setText ( mData.get(position));
                ContentInfo info = mData.get(position);
                ContentViewHolder contentViewHolder = (ContentViewHolder) holder;
                contentViewHolder.tv_channel_ame.setText(info.getChannelName()+"");
                contentViewHolder.tv_content.setText(info.getEpgStartTime()+" "+info.getContentName());
                if("0".equals(info.getContentStatus())) {  //回看
                    contentViewHolder.tv_content_status.setText("回看");
                    contentViewHolder.tv_people_count.setText(info.getPeopleCount()+"人看过");
                    contentViewHolder.tv_content_status.setTextColor(ContextCompat.getColor(mContext,R.color.black));
                } else if("1".equals(info.getContentStatus()) ) {  //直播中
                    contentViewHolder.tv_content_status.setText("直播中");
                    contentViewHolder.tv_content_status.setBackgroundResource(R.drawable.corner_orange);
                    contentViewHolder.tv_content_status.setTextColor(ContextCompat.getColor(mContext,R.color.white));
                    contentViewHolder.tv_people_count.setText(info.getPeopleCount()+"人在看");
                } else if("2".equals(info.getContentStatus())) {  //预定
                    contentViewHolder.tv_content_status.setText("预订");
                    contentViewHolder.tv_people_count.setText(info.getPeopleCount()+"人预订");
                    contentViewHolder.tv_content_status.setTextColor(ContextCompat.getColor(mContext,R.color.black));
                }
                if(info.getImageUrlList().size()>0) {
                    GlideImgManager.loadImage(mContext,info.getImageUrlList().get(0).getImageUrl(),contentViewHolder.iv_cover);
                }

                break;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        int viewType;
        if(!TextUtils.isEmpty(mData.get(position).getTitle())) {
            viewType = TYPE_TITLE;
        } else {
            viewType = TYPE_CONTENT;
        }

        return  viewType;
    }



    public class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_channel_ame;
        TextView tv_content;
        TextView tv_people_count;
        TextView tv_content_status;
        ImageView iv_cover;
        public ContentViewHolder ( View itemView ) {
            super(itemView);
            tv_channel_ame= (TextView) itemView.findViewById(R.id.tv_channel_ame);
            tv_content= (TextView) itemView.findViewById(R.id.tv_content);
            tv_people_count= (TextView) itemView.findViewById(R.id.tv_people_count);
            tv_content_status= (TextView) itemView.findViewById(R.id.tv_content_status);
            iv_cover = (ImageView) itemView.findViewById(R.id.iv_cover);
        }

    }
    public class TitleViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        public TitleViewHolder ( View itemView ) {
            super(itemView );
            mTitle= (TextView) itemView.findViewById(R.id.tv_header_title);
        }
    }
}
