package com.example.pc.gudou.fragment;

import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.pc.gudou.R;
import com.example.pc.gudou.adapter.LiveAdapter;
import com.example.pc.gudou.adapter.RecommendAdapter;
import com.example.pc.gudou.adapter.SubComlumnAdapter;
import com.example.pc.gudou.common.BaseFragment;
import com.example.pc.gudou.entity.ContentInfo;
import com.example.pc.gudou.entity.Response;
import com.example.pc.gudou.entity.SubColumnInfo;
import com.example.pc.gudou.util.GlideImageLoader;
import com.example.pc.gudou.widgets.NoScrollGridView;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by pc on 2017/5/26.
 */

public class LiveFragment extends BaseFragment {
    private static final int REFRESH_COMPLETE = 0xff01;
    private ArrayList<ContentInfo> listData = new ArrayList<>();
    private XRecyclerView mRecyclerView;
    private LiveAdapter mAdapter;
    private NoScrollGridView gv_item;

    private int page = 1;

//    Handler handler = new Handler() {
//        @Override
//
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case REFRESH_COMPLETE:
//                    mAdapter.notifyDataSetChanged();
//                    mRecyclerView.refreshComplete();
//                    break;
//            }
//        }
//    };


    @Override
    public int getLayout() {return R.layout.fragment_live;
    }

    @Override
    public void initViews(View view) {
        initBanner();
        initList();
    }



    private List<SubColumnInfo> getSubComlumn() {
        String json = "{\n" +
                "\t\"status\":\"\",\n" +
                "\t\"errorMessage\":\"\",\n" +
                "\t\"data\":{\n" +
                "\t\t\"subColumnInfos\":[\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"subColumnID\":\"1\",\n" +
                "\t\t\t\t\"subColumnName\":\"全部\",\n" +
                "\t\t\t\t\"showFlg\":\"1\",\n" +
                "\t\t\t\t\"subColumnType\":\"1\",\n" +
                "\t\t\t\t\"sortInx\":\"0\",\n" +
                "\t\t\t\t\"imageUrl\":\"http://www.easyicon.net/api/resizeApi.php?id=1206071&size=128\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"subColumnID\":\"2\",\n" +
                "\t\t\t\t\"subColumnName\":\"央视\",\n" +
                "\t\t\t\t\"showFlg\":\"1\",\n" +
                "\t\t\t\t\"subColumnType\":\"1\",\n" +
                "\t\t\t\t\"sortInx\":\"1\",\n" +
                "\t\t\t\t\"imageUrl\":\"http://www.easyicon.net/api/resizeApi.php?id=1206071&size=128\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\t\t\t{\n" +
                "\t\t\t\t\"subColumnID\":\"3\",\n" +
                "\t\t\t\t\"subColumnName\":\"卫视\",\n" +
                "\t\t\t\t\"showFlg\":\"1\",\n" +
                "\t\t\t\t\"subColumnType\":\"1\",\n" +
                "\t\t\t\t\"sortInx\":\"1\",\n" +
                "\t\t\t\t\"imageUrl\":\"http://www.easyicon.net/api/resizeApi.php?id=1206071&size=128\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\t\t\t{\n" +
                "\t\t\t\t\"subColumnID\":\"4\",\n" +
                "\t\t\t\t\"subColumnName\":\"少儿\",\n" +
                "\t\t\t\t\"showFlg\":\"1\",\n" +
                "\t\t\t\t\"subColumnType\":\"1\",\n" +
                "\t\t\t\t\"sortInx\":\"1\",\n" +
                "\t\t\t\t\"imageUrl\":\"http://www.easyicon.net/api/resizeApi.php?id=1206071&size=128\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\t\t\t{\n" +
                "\t\t\t\t\"subColumnID\":\"5\",\n" +
                "\t\t\t\t\"subColumnName\":\"地方\",\n" +
                "\t\t\t\t\"showFlg\":\"1\",\n" +
                "\t\t\t\t\"subColumnType\":\"1\",\n" +
                "\t\t\t\t\"sortInx\":\"1\",\n" +
                "\t\t\t\t\"imageUrl\":\"http://www.easyicon.net/api/resizeApi.php?id=1206071&size=128\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\t\t\t{\n" +
                "\t\t\t\t\"subColumnID\":\"6\",\n" +
                "\t\t\t\t\"subColumnName\":\"影视\",\n" +
                "\t\t\t\t\"showFlg\":\"1\",\n" +
                "\t\t\t\t\"subColumnType\":\"1\",\n" +
                "\t\t\t\t\"sortInx\":\"1\",\n" +
                "\t\t\t\t\"imageUrl\":\"http://www.easyicon.net/api/resizeApi.php?id=1206071&size=128\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\t\t\t{\n" +
                "\t\t\t\t\"subColumnID\":\"7\",\n" +
                "\t\t\t\t\"subColumnName\":\"体育\",\n" +
                "\t\t\t\t\"showFlg\":\"1\",\n" +
                "\t\t\t\t\"subColumnType\":\"1\",\n" +
                "\t\t\t\t\"sortInx\":\"1\",\n" +
                "\t\t\t\t\"imageUrl\":\"http://www.easyicon.net/api/resizeApi.php?id=1206071&size=128\"\n" +
                "\t\t\t},\t\t\t{\n" +
                "\t\t\t\t\"subColumnID\":\"8\",\n" +
                "\t\t\t\t\"subColumnName\":\"综艺\",\n" +
                "\t\t\t\t\"showFlg\":\"1\",\n" +
                "\t\t\t\t\"subColumnType\":\"1\",\n" +
                "\t\t\t\t\"sortInx\":\"1\",\n" +
                "\t\t\t\t\"imageUrl\":\"http://www.easyicon.net/api/resizeApi.php?id=1206071&size=128\"\n" +
                "\t\t\t}\n" +
                "\t\t]\n" +
                "\t}\t\n" +
                "}";
        List<SubColumnInfo> subColumnInfos = new ArrayList<>();
        try {
            Response jsonData = new Gson().fromJson(json, Response.class);
            subColumnInfos = jsonData.getData().getSubColumnInfos();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return subColumnInfos;
    }


    /**
     * 初始化广告轮播
     */
    private void initBanner() {
        final List<ContentInfo> contentInfos;
        List<String> titles = new ArrayList<>();
        List<String> images = new ArrayList<>();
        contentInfos = getBanner();
        for (int i = 0; i < contentInfos.size(); i++) {
            titles.add(contentInfos.get(i).getContentName());
            images.add(contentInfos.get(i).getImageUrl());
        }

        Banner banner = (Banner) findViewById(R.id.banner);
        //设置图片加载器
        Banner banner1 = banner.setImageLoader(new GlideImageLoader());
        //设置图片集合
        banner.setImages(images);
        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR_TITLE_INSIDE);
        banner.setBannerTitles(titles);
        banner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                String url = contentInfos.get(position).getTargetUrl();
                Log.i("tes", "url: " + url);
            }
        });
        //banner设置方法全部调用完毕时最后调用
        banner.start();
    }


    /**
     * 初始化列表数据
     */
    private void initList() {
        View header =   LayoutInflater.from(getActivity()).inflate(R.layout.head_subcomlumn, null);

         mRecyclerView = (XRecyclerView) this.findViewById(R.id.recyclerview);
        gv_item = (NoScrollGridView) header.findViewById(R.id.gv_item);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
//        barLayout = findViewById(R.id.appbar);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);


        mAdapter = new LiveAdapter(getActivity(), listData);

//        for (int i = 0; i < 15; i++) {
//            listData.add("item" + i);
//        }

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader);
        mRecyclerView.setLoadingMoreProgressStyle(ProgressStyle.Pacman);
        mRecyclerView.setArrowImageView(R.drawable.tab_relation_normal);


        mRecyclerView.addHeaderView(header);
        List<SubColumnInfo> subColumnInfos = new ArrayList<>();
        subColumnInfos = getSubComlumn();
        SubComlumnAdapter subComlumnAdapter = new SubComlumnAdapter(getActivity(), subColumnInfos, R.layout.item_subcomlumn);
        gv_item.setAdapter(subComlumnAdapter);

        refreshData();
        //View header = LayoutInflater.from(getActivity()).inflate(R.layout.recyclerview_header, (ViewGroup) findViewById(android.R.id.content), false);
        //  mRecyclerView.addHeaderView(header);
        mRecyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                page = 1;
                new Handler().postDelayed(new Runnable() {
                    public void run() {

                        refreshData();

                    }

                }, 3000);            //refresh data here
            }

            @Override
            public void onLoadMore() {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        refreshData();
                    }
                }, 3000);
            }
        });
    }

    /**
     * 刷新数据
     */
    private void refreshData() {
        String json = "{\n" +
                "\t\"status\":\"\",\n" +
                "\t\"errorMessage\":\"\",\n" +
                "\t\"data\":{\n" +
                "\t\t\"columnSize\":\"10\",\n" +
                "\t\t\"contentInfos\":[\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"1\",\n" +
                "\t\t\t\t\"contentID\":\"11002_GDZX2320170329001383_package\",\n" +
                "\t\t\t\t\"channelID\":\"\",\n" +
                "\t\t\t\t\"channelName\":\"\",\n" +
                "\t\t\t\t\"epgStartTime\":\"\",\n" +
                "\t\t\t\t\"epgEndTime\":\"\",\n" +
                "\t\t\t\t\"contentStatus\":\"1\",\n" +
                "\t\t\t\t\"contentName\":\"那年我们青春正好\",\n" +
                "\t\t\t\t\"contentDes\":\"testForContentListBySubColumnID\",\n" +
                "\t\t\t\t\"peopleCount\":\"100\",\n" +
                "\t\t\t\t\"updateTime\":\"15:00\",\n" +
                "\t\t\t\t\"contentType\":\"2\",\n" +
                "\t\t\t\t\"sortInx\":\"1\",\n" +
                "\t\t\t\t\"imageUrlFlg\":\"1\",\n" +
                "\t\t\t\t\"imageUrlList\":[\n" +
                "\t\t\t\t\t{\n" +
                "\t\t\t\t\t\t\"ImageUrl\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494577668868&di=dda891fce9fe0732014173c0e3a4b0ca&imgtype=0&src=http%3A%2F%2Fwww.sinaimg.cn%2Fdy%2Fslidenews%2F4_img%2F2012_34%2F703_731460_296623.jpg\"\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t],\n" +
                "\t\t\t\t\"targetUrl\":\"https://www.baidu.com\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"1\",\n" +
                "\t\t\t\t\"contentID\":\"11002_GDZX2320170329001383_package\",\n" +
                "\t\t\t\t\"channelID\":\"\",\n" +
                "\t\t\t\t\"channelName\":\"CCTV5\",\n" +
                "\t\t\t\t\"epgStartTime\":\"\",\n" +
                "\t\t\t\t\"epgEndTime\":\"\",\n" +
                "\t\t\t\t\"contentStatus\":\"0\",\n" +
                "\t\t\t\t\"contentName\":\"花千骨\",\n" +
                "\t\t\t\t\"contentDes\":\"testForContentListBySubColumnID\",\n" +
                "\t\t\t\t\"peopleCount\":\"100\",\n" +
                "\t\t\t\t\"updateTime\":\"15:00\",\n" +
                "\t\t\t\t\"contentType\":\"2\",\n" +
                "\t\t\t\t\"sortInx\":\"2\",\n" +
                "\t\t\t\t\"imageUrlFlg\":\"1\",\n" +
                "\t\t\t\t\"imageUrlList\":[\n" +
                "\t\t\t\t\t{\n" +
                "\t\t\t\t\t\t\"ImageUrl\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1495173727&di=58da65fb252d2f6b1eb8db7034fbc8fd&imgtype=jpg&er=1&src=http%3A%2F%2Fimg.zcool.cn%2Fcommunity%2F016b025732cf480000002631e3574e.jpg%40900w_1l_2o_100sh.jpg\"\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t],\n" +
                "\t\t\t\t\"targetUrl\":\"https://www.baidu.com\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"1\",\n" +
                "\t\t\t\t\"contentID\":\"11002_GDZX2320170329001383_package\",\n" +
                "\t\t\t\t\"channelID\":\"\",\n" +
                "\t\t\t\t\"channelName\":\"\",\n" +
                "\t\t\t\t\"epgStartTime\":\"\",\n" +
                "\t\t\t\t\"epgEndTime\":\"\",\n" +
                "\t\t\t\t\"contentStatus\":\"0\",\n" +
                "\t\t\t\t\"contentName\":\"女医明妃传\",\n" +
                "\t\t\t\t\"contentDes\":\"testForContentListBySubColumnID\",\n" +
                "\t\t\t\t\"peopleCount\":\"100\",\n" +
                "\t\t\t\t\"updateTime\":\"15:00\",\n" +
                "\t\t\t\t\"contentType\":\"2\",\n" +
                "\t\t\t\t\"sortInx\":\"3\",\n" +
                "\t\t\t\t\"imageUrlFlg\":\"1\",\n" +
                "\t\t\t\t\"imageUrlList\":[\n" +
                "\t\t\t\t\t{\n" +
                "\t\t\t\t\t\t\"ImageUrl\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494579012912&di=cce67372613b65a6c4758c68bd078a22&imgtype=0&src=http%3A%2F%2Fimg1.gtimg.com%2Fsports%2Fpics%2Fhv1%2F193%2F209%2F2032%2F132184288.jpg\"\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t],\n" +
                "\t\t\t\t\"targetUrl\":\"https://www.baidu.com\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"1\",\n" +
                "\t\t\t\t\"contentID\":\"11002_GDZX2320170329001383_package\",\n" +
                "\t\t\t\t\"channelID\":\"\",\n" +
                "\t\t\t\t\"channelName\":\"\",\n" +
                "\t\t\t\t\"epgStartTime\":\"201705121500\",\n" +
                "\t\t\t\t\"epgEndTime\":\"201705121500\",\n" +
                "\t\t\t\t\"contentStatus\":\"2\",\n" +
                "\t\t\t\t\"contentName\":\"人民的名义\",\n" +
                "\t\t\t\t\"contentDes\":\"testForContentListBySubColumnID\",\n" +
                "\t\t\t\t\"peopleCount\":\"100\",\n" +
                "\t\t\t\t\"updateTime\":\"15:00\",\n" +
                "\t\t\t\t\"contentType\":\"2\",\n" +
                "\t\t\t\t\"sortInx\":\"4\",\n" +
                "\t\t\t\t\"imageUrlFlg\":\"1\",\n" +
                "\t\t\t\t\"imageUrlList\":[\n" +
                "\t\t\t\t\t{\n" +
                "\t\t\t\t\t\t\"ImageUrl\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494579107016&di=9b049e5a044094a2d5ff8fbffc8432a0&imgtype=0&src=http%3A%2F%2Fwww.sznews.com%2Fsports%2Fimages%2Fattachement%2Fjpg%2Fsite3%2F20151217%2F002185601eb817dcc56b62.jpg\"\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t],\n" +
                "\t\t\t\t\"targetUrl\":\"https://www.baidu.com\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"1\",\n" +
                "\t\t\t\t\"contentID\":\"11002_GDZX2320170329001383_package\",\n" +
                "\t\t\t\t\"channelID\":\"\",\n" +
                "\t\t\t\t\"channelName\":\"\",\n" +
                "\t\t\t\t\"epgStartTime\":\"201705121500\",\n" +
                "\t\t\t\t\"epgEndTime\":\"201705121500\",\n" +
                "\t\t\t\t\"contentStatus\":\"2\",\n" +
                "\t\t\t\t\"contentName\":\"小猪佩奇\",\n" +
                "\t\t\t\t\"contentDes\":\"testForContentListBySubColumnID\",\n" +
                "\t\t\t\t\"peopleCount\":\"100\",\n" +
                "\t\t\t\t\"updateTime\":\"15:00\",\n" +
                "\t\t\t\t\"contentType\":\"2\",\n" +
                "\t\t\t\t\"sortInx\":\"5\",\n" +
                "\t\t\t\t\"imageUrlFlg\":\"1\",\n" +
                "\t\t\t\t\"imageUrlList\":[\n" +
                "\t\t\t\t\t{\n" +
                "\t\t\t\t\t\t\"ImageUrl\":\"https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3357038007,3562105673&fm=23&gp=0.jpg\"\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t],\n" +
                "\t\t\t\t\"targetUrl\":\"https://www.baidu.com\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"1\",\n" +
                "\t\t\t\t\"contentID\":\"11002_GDZX2320170329001383_package\",\n" +
                "\t\t\t\t\"channelID\":\"\",\n" +
                "\t\t\t\t\"channelName\":\"\",\n" +
                "\t\t\t\t\"epgStartTime\":\"201705121500\",\n" +
                "\t\t\t\t\"epgEndTime\":\"201705121500\",\n" +
                "\t\t\t\t\"contentStatus\":\"2\",\n" +
                "\t\t\t\t\"contentName\":\"钢铁巨人\",\n" +
                "\t\t\t\t\"contentDes\":\"testForContentListBySubColumnID\",\n" +
                "\t\t\t\t\"peopleCount\":\"100\",\n" +
                "\t\t\t\t\"updateTime\":\"15:00\",\n" +
                "\t\t\t\t\"contentType\":\"2\",\n" +
                "\t\t\t\t\"sortInx\":\"5\",\n" +
                "\t\t\t\t\"imageUrlFlg\":\"1\",\n" +
                "\t\t\t\t\"imageUrlList\":[\n" +
                "\t\t\t\t\t{\n" +
                "\t\t\t\t\t\t\"ImageUrl\":\"https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3357038007,3562105673&fm=23&gp=0.jpg\"\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t],\n" +
                "\t\t\t\t\"targetUrl\":\"https://www.baidu.com\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"1\",\n" +
                "\t\t\t\t\"contentID\":\"11002_GDZX6120170503000214_package\",\n" +
                "\t\t\t\t\"channelID\":\"\",\n" +
                "\t\t\t\t\"channelName\":\"\",\n" +
                "\t\t\t\t\"epgStartTime\":\"201705121500\",\n" +
                "\t\t\t\t\"epgEndTime\":\"201705121500\",\n" +
                "\t\t\t\t\"contentStatus\":\"2\",\n" +
                "\t\t\t\t\"contentName\":\"顶部推荐1\",\n" +
                "\t\t\t\t\"contentDes\":\"testForContentListBySubColumnID\",\n" +
                "\t\t\t\t\"peopleCount\":\"100\",\n" +
                "\t\t\t\t\"updateTime\":\"15:00\",\n" +
                "\t\t\t\t\"contentType\":\"2\",\n" +
                "\t\t\t\t\"sortInx\":\"5\",\n" +
                "\t\t\t\t\"imageUrlFlg\":\"1\",\n" +
                "\t\t\t\t\"imageUrlList\":[\n" +
                "\t\t\t\t\t{\n" +
                "\t\t\t\t\t\t\"ImageUrl\":\"https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3357038007,3562105673&fm=23&gp=0.jpg\"\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t],\n" +
                "\t\t\t\t\"targetUrl\":\"https://www.baidu.com\"\n" +
                "\t\t\t}\n" +
                "\t\t]\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "}";
        List<ContentInfo> contentInfos = new ArrayList<>();
        try {
            Response jsonData = new Gson().fromJson(json, Response.class);
            contentInfos = jsonData.getData().getContentInfos();
            if (page == 1) {
                listData.clear();
            }
            listData.addAll(contentInfos);
            ContentInfo title1 = new ContentInfo();
            title1.setTitle("推荐");
            ContentInfo title2 = new ContentInfo();
            title2.setTitle("风风火火");
            listData.add(0,title1);
            listData.add(4,title2);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
          //handler.sendEmptyMessage(REFRESH_COMPLETE);
            mAdapter.notifyDataSetChanged();
            mRecyclerView.refreshComplete();
        }

    }


    /**
     * 获取广告图
     *
     * @return
     */
    private List<ContentInfo> getBanner() {
        List<ContentInfo> contentInfos = new ArrayList<>();
        String json = "{\n" +
                "\t\"status\":\"0\",\n" +
                "\t\"errorMessage\":\"\",\n" +
                "\t\"data\":{\n" +
                "\t\t\"contentInfos\":[\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"1\",\n" +
                "\t\t\t\t\"contentID\":\"1\",\n" +
                "\t\t\t\t\"contentName\":\"图文类型\",\n" +
                "\t\t\t\t\"contentType\":\"1\",\n" +
                "\t\t\t\t\"sortInx\":\"0\",\n" +
                "\t\t\t\t\"imageUrl\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494328935490&di=23423da763def47aaeba234a8ebfb561&imgtype=0&src=http%3A%2F%2Fwww.qiuyiweb.com%2Fwp-content%2Fuploads%2F2016%2F08%2F1470018849-3036-146986955797094100-a580xH.jpg\",\n" +
                "\t\t\t\t\"targetUrl\":\"https://www.baidu.com\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"1\",\n" +
                "\t\t\t\t\"contentID\":\"11002_GDZX2320170329001383_package\",\n" +
                "\t\t\t\t\"contentName\":\"点播类型\",\n" +
                "\t\t\t\t\"contentType\":\"2\",\n" +
                "\t\t\t\t\"sortInx\":\"2\",\n" +
                "\t\t\t\t\"imageUrl\":\"https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1494318842&di=0757262801afc5b60befa774327f2559&src=http://imgsrc.baidu.com/forum/pic/item/78310a55b319ebc4e12ee5028226cffc1f1716d2.jpg\",\n" +
                "\t\t\t\t\"targetUrl\":\"\"\n" +
                "\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"2\",\n" +
                "\t\t\t\t\"contentID\":\"31b2e7223d614ba0aecbfba72cfc360e\",\n" +
                "\t\t\t\t\"contentName\":\"直播类型\",\n" +
                "\t\t\t\t\"contentType\":\"3\",\n" +
                "\t\t\t\t\"sortInx\":\"3\",\n" +
                "\t\t\t\t\"imageUrl\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494329006365&di=ed8e57b5c89f22397dd4deb3f3bef5b3&imgtype=0&src=http%3A%2F%2Fwww.90oo.com%2Fbbs88%2Fattachments%2Fmonth_1605%2F1605120510417c4b2d98f82857.jpg\",\n" +
                "\t\t\t\t\"targetUrl\":\"\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"1\",\n" +
                "\t\t\t\t\"contentID\":\"1\",\n" +
                "\t\t\t\t\"contentName\":\"纯图类型\",\n" +
                "\t\t\t\t\"contentType\":\"4\",\n" +
                "\t\t\t\t\"sortInx\":\"4\",\n" +
                "\t\t\t\t\"imageUrl\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494329006364&di=170e99b53b84fa3f4e37178a257d8434&imgtype=0&src=http%3A%2F%2Fwww.wallcoo.com%2Fsport%2FPremier_League_Arsenal_2010-11%2Fwallpapers%2F1680x1050%2FArsenal_gun__1280475147.jpg\",\n" +
                "\t\t\t\t\"targetUrl\":\"\"\t\t\t\t\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"ID\":\"1\",\n" +
                "\t\t\t\t\"contentID\":\"1\",\n" +
                "\t\t\t\t\"contentName\":\"广告类型\",\n" +
                "\t\t\t\t\"contentType\":\"5\",\n" +
                "\t\t\t\t\"sortInx\":\"5\",\n" +
                "\t\t\t\t\"imageUrl\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494329006363&di=3a760b7d81bd7142ce2704d8103467b7&imgtype=0&src=http%3A%2F%2Fimg1.gtimg.com%2Fsports%2Fpics%2Fhv1%2F34%2F223%2F1986%2F129196549.png\",\n" +
                "\t\t\t\t\"targetUrl\":\"\"\n" +
                "\t\t\t}\n" +
                "\t\t]\n" +
                "\t}\n" +
                "}";

        try {
            Response jsonData = new Gson().fromJson(json, Response.class);
            contentInfos = jsonData.getData().getContentInfos();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentInfos;
    }
}
