package com.example.pc.gudou.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.example.pc.gudou.R;
import com.example.pc.gudou.common.BaseFragment;
import com.example.pc.gudou.entity.ColumnInfo;
import com.example.pc.gudou.entity.Response;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pc on 2017/5/25.
 */

public class HomeFragment extends BaseFragment {
    private ViewPager viewPager;
    private List<String> titles = new ArrayList<>();
    private FragmentAdapter viewPagerAdapter;
    private List<Fragment> fragments;
    private List<ColumnInfo> columnInfos;
    private TabLayout tabLayout;

    @Override
    public int getLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public void initViews(View view) {
        viewPager = findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);

        columnInfos = getTitles();
        fragments = new ArrayList<>();

        for (int i = 0; i < columnInfos.size(); i++) {

            if (i == 0) {
                fragments.add(RecommendFragment.newInstance(columnInfos.get(i)));
            } else if (i == 1) {
                fragments.add(new LiveFragment());
            } else if (i == 2) {
                fragments.add(new SwoingFragment());

            } else if (i == 3) {
                fragments.add(new CityFragment());

            } else if (i == 4) {
                fragments.add(new CityFragment());

            } else {
                fragments.add(new CityFragment());
            }

            titles.add(columnInfos.get(i).getColumnName());
        }

        viewPagerAdapter = new FragmentAdapter(getChildFragmentManager(),fragments,titles);
        viewPager.setAdapter(viewPagerAdapter);
        // slidingTabLayout.setViewPager(viewPager,titles);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        //navitationLayout.setViewPager(getActivity(), titles ,viewPager, R.color.color_333333, R.color.color_2581ff, 13, 13, 0, 0, true);
        // navitationLayout.setBgLine(getActivity(), 1, R.color.colorAccent);
//        int w = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
//        int h = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
//        navitationLayout.measure(w, h);
//        int width =navitationLayout.getMeasuredWidth();
//        int widthPx =  navitationLayout.dip2px(getActivity(),width);
//        navitationLayout.setNavLine(getActivity(), 1, R.color.colorPrimary, 0,widthPx);
    }

    public List<ColumnInfo> getTitles() {
        List<ColumnInfo> columnInfos = new ArrayList<>();
//        OkHttpUtils
//                .postString()
//                .url(url)
//                .content(new Gson().toJson(new User("zhy", "123")))
//                .mediaType(MediaType.parse("application/json; charset=utf-8"))
//                .build()
//                .execute(new MyStringCallback());
        String string = getString(R.string.testForHomeColumnList);
        try {
            Response jsonData = new Gson().fromJson("{\n" +
                    "\t\"status\":\"0\",\n" +
                    "\t\"errorMessage\":\"\",\n" +
                    "\t\"data\":{\n" +
                    "\t\t\"columnInfos\":[\n" +
                    "\t\t\t{\n" +
                    "\t\t\t\t\"columnID\":\"1\",\n" +
                    "\t\t\t\t\"columnName\":\"推荐\",\n" +
                    "\t\t\t\t\"showFlg\":\"1\",\n" +
                    "\t\t\t\t\"columnType\":\"1\",\n" +
                    "\t\t\t\t\"sortInx\":\"1\"\n" +
                    "\t\t\t},\n" +
                    "\t\t\t{\n" +
                    "\t\t\t\t\"columnID\":\"2\",\n" +
                    "\t\t\t\t\"columnName\":\"直播\",\n" +
                    "\t\t\t\t\"showFlg\":\"1\",\n" +
                    "\t\t\t\t\"columnType\":\"3\",\n" +
                    "\t\t\t\t\"sortInx\":\"2\"\n" +
                    "\t\t\t},\n" +
                    "\t\t\t{\n" +
                    "\t\t\t\t\"columnID\":\"3\",\n" +
                    "\t\t\t\t\"columnName\":\"点播\",\n" +
                    "\t\t\t\t\"showFlg\":\"1\",\n" +
                    "\t\t\t\t\"columnType\":\"2\",\n" +
                    "\t\t\t\t\"sortInx\":\"3\"\n" +
                    "\t\t\t},\n" +
                    "\t\t\t{\n" +
                    "\t\t\t\t\"columnID\":\"4\",\n" +
                    "\t\t\t\t\"columnName\":\"本地\",\n" +
                    "\t\t\t\t\"showFlg\":\"1\",\n" +
                    "\t\t\t\t\"columnType\":\"4\",\n" +
                    "\t\t\t\t\"sortInx\":\"4\"\t\t\t\t\n" +
                    "\t\t\t},\n" +
                    "\t\t\t{\n" +
                    "\t\t\t\t\"columnID\":\"4\",\n" +
                    "\t\t\t\t\"columnName\":\"本地\",\n" +
                    "\t\t\t\t\"showFlg\":\"1\",\n" +
                    "\t\t\t\t\"columnType\":\"4\",\n" +
                    "\t\t\t\t\"sortInx\":\"4\"\t\t\t\t\n" +
                    "\t\t\t},\n" +
                    "\t\t\t{\n" +
                    "\t\t\t\t\"columnID\":\"4\",\n" +
                    "\t\t\t\t\"columnName\":\"本地\",\n" +
                    "\t\t\t\t\"showFlg\":\"1\",\n" +
                    "\t\t\t\t\"columnType\":\"4\",\n" +
                    "\t\t\t\t\"sortInx\":\"4\"\t\t\t\t\n" +
                    "\t\t\t},\n" +
                    "\t\t\t{\n" +
                    "\t\t\t\t\"columnID\":\"4\",\n" +
                    "\t\t\t\t\"columnName\":\"本地\",\n" +
                    "\t\t\t\t\"showFlg\":\"1\",\n" +
                    "\t\t\t\t\"columnType\":\"4\",\n" +
                    "\t\t\t\t\"sortInx\":\"4\"\t\t\t\t\n" +
                    "\t\t\t},\n" +
                    "\t\t\t{\n" +
                    "\t\t\t\t\"columnID\":\"4\",\n" +
                    "\t\t\t\t\"columnName\":\"本地\",\n" +
                    "\t\t\t\t\"showFlg\":\"1\",\n" +
                    "\t\t\t\t\"columnType\":\"4\",\n" +
                    "\t\t\t\t\"sortInx\":\"4\"\t\t\t\t\n" +
                    "\t\t\t},\n" +
                    "\t\t\t{\n" +
                    "\t\t\t\t\"columnID\":\"4\",\n" +
                    "\t\t\t\t\"columnName\":\"本地\",\n" +
                    "\t\t\t\t\"showFlg\":\"1\",\n" +
                    "\t\t\t\t\"columnType\":\"4\",\n" +
                    "\t\t\t\t\"sortInx\":\"4\"\t\t\t\t\n" +
                    "\t\t\t}\n" +
                    "\t\t]\n" +
                    "\t}\n" +
                    "}", Response.class);
            columnInfos = jsonData.getData().getColumnInfos();
//            titles = new String[columnInfos.size()];
//            if(columnInfos!=null&&columnInfos.size()>0){
//                for(int i=0;i<columnInfos.size();i++) {
//                    titles[i] = columnInfos.get(i).getColumnName();
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return columnInfos;
    }


   class FragmentAdapter extends FragmentPagerAdapter {


        private List<Fragment> list_fragment;                         //fragment列表

        private List<String> list_Title;                              //tab名的列表


        public FragmentAdapter(FragmentManager fm, List<Fragment> list_fragment, List<String> list_Title) {

            super(fm);

            this.list_fragment = list_fragment;

            this.list_Title = list_Title;

        }


        @Override

        public Fragment getItem(int i) {

            return list_fragment.get(i);

        }


        @Override

        public int getCount() {

            return list_fragment.size();

        }


        @Override

        public CharSequence getPageTitle(int position) {

            return list_Title.get(position % list_Title.size());

        }

    }

}
