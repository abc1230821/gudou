package com.example.pc.gudou.entity;

import java.util.List;

/**
 * Created by Administrator on 2017/5/31.
 */

public class ContentInfo extends Entity {
    private String ID;
    private String contentID;
    private String contentName;
    private int contentType;
    private String sortInx;
    private String imageUrl ;
    private String targetUrl;
    private String channelID;
    private String channelName;
    private String epgStartTime;
    private String epgEndTime;
    private String contentStatus;
    private String contentDes;
    private String peopleCount;
    private String updateTime;
    private int imageUrlFlg;
    private List<ImageUrl> imageUrlList;
    public String getContentID() {
        return contentID;
    }
    public String  title;
    public void setContentID(String contentID) {
        this.contentID = contentID;
    }

    public String getContentName() {
        return contentName;
    }

    public void setContentName(String contentName) {
        this.contentName = contentName;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public String getSortInx() {
        return sortInx;
    }

    public void setSortInx(String sortInx) {
        this.sortInx = sortInx;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public String getChannelID() {
        return channelID;
    }

    public void setChannelID(String channelID) {
        this.channelID = channelID;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getEpgStartTime() {
        return epgStartTime;
    }

    public void setEpgStartTime(String epgStartTime) {
        this.epgStartTime = epgStartTime;
    }

    public String getEpgEndTime() {
        return epgEndTime;
    }

    public void setEpgEndTime(String epgEndTime) {
        this.epgEndTime = epgEndTime;
    }

    public String getContentStatus() {
        return contentStatus;
    }

    public void setContentStatus(String contentStatus) {
        this.contentStatus = contentStatus;
    }

    public String getContentDes() {
        return contentDes;
    }

    public void setContentDes(String contentDes) {
        this.contentDes = contentDes;
    }

    public String getPeopleCount() {
        return peopleCount;
    }

    public void setPeopleCount(String peopleCount) {
        this.peopleCount = peopleCount;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public int getImageUrlFlg() {
        return imageUrlFlg;
    }

    public void setImageUrlFlg(int imageUrlFlg) {
        this.imageUrlFlg = imageUrlFlg;
    }

    public List<ImageUrl> getImageUrlList() {
        return imageUrlList;
    }

    public void setImageUrlList(List<ImageUrl> imageUrlList) {
        this.imageUrlList = imageUrlList;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
