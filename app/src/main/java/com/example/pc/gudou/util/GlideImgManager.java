package com.example.pc.gudou.util;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.pc.gudou.R;

import java.io.File;

/**
 * Created by Administrator on 2017/6/1.
 */

public class GlideImgManager {

    public static void loadImage(Context context, String path, final ImageView imageView) {
        Glide.with(context).load(path).into(imageView);
    }
}
