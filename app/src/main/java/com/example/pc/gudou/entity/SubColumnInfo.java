package com.example.pc.gudou.entity;

/**
 * Created by Administrator on 2017/5/31.
 */

public class SubColumnInfo extends Entity {
    private String subColumnID;
    private String subColumnName;
    private int showFlg;
    private int subColumnType;
    private String sortInx;

    private String imageUrl;
    public String getSubColumnID() {
        return subColumnID;
    }

    public void setSubColumnID(String subColumnID) {
        this.subColumnID = subColumnID;
    }

    public String getSubColumnName() {
        return subColumnName;
    }

    public void setSubColumnName(String subColumnName) {
        this.subColumnName = subColumnName;
    }

    public int getShowFlg() {
        return showFlg;
    }

    public void setShowFlg(int showFlg) {
        this.showFlg = showFlg;
    }

    public int getSubColumnType() {
        return subColumnType;
    }

    public void setSubColumnType(int subColumnType) {
        this.subColumnType = subColumnType;
    }

    public String getSortInx() {
        return sortInx;
    }

    public void setSortInx(String sortInx) {
        this.sortInx = sortInx;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
